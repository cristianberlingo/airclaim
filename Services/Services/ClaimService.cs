using System.Collections.Generic;
using AirClaimAPI.DAL.RepositoriesContracts;
using AirClaimAPI.Models;
using AirClaimAPI.Services.ServicesContracts;

namespace AirClaimAPI.Services.Services
{
    public class ClaimService : IBaseService<Claim>
    {
        private readonly IClaimRepository _claimRepository;

        public ClaimService(IClaimRepository claimRepository)
        {
            _claimRepository = claimRepository;
        }

        public void Create(Claim claim)
        {
            _claimRepository.Insert(claim);
        }

        public void Delete(int id)
        {
            _claimRepository.Delete(id);
        }

        public List<Claim> Get()
        {
            return _claimRepository.Get();
        }

        public Claim GetById(int id)
        {
            return _claimRepository.GetById(id);
        }

        public void Update(Claim claim)
        {
            _claimRepository.Update(claim);
        }
    }
}