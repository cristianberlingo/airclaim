using System.Collections.Generic;
using AirClaimAPI.DAL.RepositoriesContracts;
using AirClaimAPI.Models;
using AirClaimAPI.Services.ServicesContracts;

namespace AirClaimAPI.Services.Services
{
    public class ClientService : IBaseService<Client>
    {
        private readonly IClientRepository _clientRepository;

        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public List<Client> Get()
        {
            return _clientRepository.Get();
        }

        public Client GetById(int id)
        {
            return _clientRepository.GetById(id);
        }

        public void Create(Client client)
        {
            _clientRepository.Insert(client);
        }

        public void Delete(int id)
        {
            _clientRepository.Delete(id);
        }

        public void Update(Client client)
        {
            _clientRepository.Update(client);
        }
        
    }
}