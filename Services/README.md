# Capa Service

Esta capa es la encargada de implementar la logica de negocio. Para esto se vale de los datos recibidos por parte del la capa Controller (enviados en las peticiones HTTP) y de los obtenidos a través de la capa de datos.

## Interface IBaseService
Esta interface funciona como abstracción de las implementaciones que pueden existir de los servicios, en nuestro ejemplo al ser que todos los servicios comparten las metodos tipicos de un CRUD la interfaz los declara.

La declaración de esta interfaz permite posteriormente inyectar el servicio en la capa Controler, eliminando el acoplamiento entre esta y la capa Service, siguiendo el principio de [Dependency inversion](https://en.wikipedia.org/wiki/Dependency_inversion_principle), facilitando asi mismo el testeo de cada capa.

## Utilización de la capa de datos
La capa Service deberia ser la unica que interactua con la capa de datos o DAL (Data Acess Layer). Para lograr esto se inyecta el o los repositorios necesitados por cada  servicio a través de su contructor.

![InyecciónRepositorio](../res/readme/service01.png)

La logica de como se impacta en la base de datos (o si el storage de datos es una base de datos o de otro tipo) es totalmente transparente para la capa de servicio, que solo debe encargar se de implementar la logica de negocio.

Por ejemplo en la eliminación de un Claim\recñ la capa de servicio solamente llama al metodo correspondiente del repositorio. No tiene conocimiento de como esto se hace y esta totalmente desacoplada de la implementación.

![Delete](../res/readme/service02.png)

Para poner un ejemplo de cuales son las responsabilidades de la capa Service si existiera la regla de negocio que impidiera borrar un Claim o reclamo si su estado es ```Judicializacion``` y en caso de borar el reclamo tener que avisar a los involucrados, la logica de esta regla deberia ser implementada por la capa de Service.