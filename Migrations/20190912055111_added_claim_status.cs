﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AirClaimAPI.Migrations
{
    public partial class added_claim_status : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Claim_Client_clientId",
                table: "Claim");

            migrationBuilder.RenameColumn(
                name: "clientId",
                table: "Claim",
                newName: "ClientId");

            migrationBuilder.RenameIndex(
                name: "IX_Claim_clientId",
                table: "Claim",
                newName: "IX_Claim_ClientId");

            migrationBuilder.AlterColumn<string>(
                name: "Value",
                table: "ContactInformation",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "ContactInformation",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Client",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Client",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "Client",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ClientId",
                table: "Claim",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "StatusId",
                table: "Claim",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "ClaimStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClaimStatus", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "ClaimStatus",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Consulta" },
                    { 2, "ReclamoViable" },
                    { 3, "EnPreparacion" },
                    { 4, "EnMediacion" },
                    { 5, "EnNegociacion" },
                    { 6, "FinalizadoAcuerdo" },
                    { 7, "PendientePago" },
                    { 8, "Judicializacion" },
                    { 9, "FinalizadoJudicial" },
                    { 10, "Pago" },
                    { 11, "CierreSinAcuerdo" },
                    { 12, "FinalizadoOtro" },
                    { 13, "ArchivoPapelera" }
                });

            migrationBuilder.UpdateData(
                table: "Claim",
                keyColumn: "Id",
                keyValue: 1,
                column: "StatusId",
                value: 1);

            migrationBuilder.CreateIndex(
                name: "IX_Claim_StatusId",
                table: "Claim",
                column: "StatusId");

            migrationBuilder.AddForeignKey(
                name: "FK_Claim_Client_ClientId",
                table: "Claim",
                column: "ClientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Claim_ClaimStatus_StatusId",
                table: "Claim",
                column: "StatusId",
                principalTable: "ClaimStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Claim_Client_ClientId",
                table: "Claim");

            migrationBuilder.DropForeignKey(
                name: "FK_Claim_ClaimStatus_StatusId",
                table: "Claim");

            migrationBuilder.DropTable(
                name: "ClaimStatus");

            migrationBuilder.DropIndex(
                name: "IX_Claim_StatusId",
                table: "Claim");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "Claim");

            migrationBuilder.RenameColumn(
                name: "ClientId",
                table: "Claim",
                newName: "clientId");

            migrationBuilder.RenameIndex(
                name: "IX_Claim_ClientId",
                table: "Claim",
                newName: "IX_Claim_clientId");

            migrationBuilder.AlterColumn<string>(
                name: "Value",
                table: "ContactInformation",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "ContactInformation",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "LastName",
                table: "Client",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "FirstName",
                table: "Client",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "Client",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<int>(
                name: "clientId",
                table: "Claim",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Claim_Client_clientId",
                table: "Claim",
                column: "clientId",
                principalTable: "Client",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
