﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AirClaimAPI.Migrations
{
    public partial class initial_create : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Dni = table.Column<int>(nullable: false),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Claim",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    clientId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Claim", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Claim_Client_clientId",
                        column: x => x.clientId,
                        principalTable: "Client",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactInformation",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimId = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactInformation", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ContactInformation_Claim_ClaimId",
                        column: x => x.ClaimId,
                        principalTable: "Claim",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Client",
                columns: new[] { "Id", "Address", "Dni", "FirstName", "LastName" },
                values: new object[] { 1, "Fake street 123", 1234567, "John", "Doe" });

            migrationBuilder.InsertData(
                table: "Client",
                columns: new[] { "Id", "Address", "Dni", "FirstName", "LastName" },
                values: new object[] { 2, "Fake street 321", 1234568, "Jane", "Doe" });

            migrationBuilder.InsertData(
                table: "Claim",
                columns: new[] { "Id", "clientId" },
                values: new object[] { 1, 1 });

            migrationBuilder.InsertData(
                table: "ContactInformation",
                columns: new[] { "Id", "ClaimId", "Description", "Value" },
                values: new object[] { 1, 1, "Cellphone", "1130959722" });

            migrationBuilder.InsertData(
                table: "ContactInformation",
                columns: new[] { "Id", "ClaimId", "Description", "Value" },
                values: new object[] { 2, 1, "Work's email", "john.doe@gmail.com" });

            migrationBuilder.CreateIndex(
                name: "IX_Claim_clientId",
                table: "Claim",
                column: "clientId");

            migrationBuilder.CreateIndex(
                name: "IX_ContactInformation_ClaimId",
                table: "ContactInformation",
                column: "ClaimId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContactInformation");

            migrationBuilder.DropTable(
                name: "Claim");

            migrationBuilder.DropTable(
                name: "Client");
        }
    }
}
