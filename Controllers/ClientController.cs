using System.Collections.Generic;
using AirClaimAPI.Models;
using AirClaimAPI.Services.ServicesContracts;
using Microsoft.AspNetCore.Mvc;

namespace AirClaimAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly IBaseService<Client> _clientService;

        public ClientController(IBaseService<Client> clientService)
        {
            _clientService = clientService;
        }

        // GET api/clients
        [HttpGet]
        public List<Client> Get()
        {
            return _clientService.Get();
        }

        // GET api/clients/5
        [HttpGet("{id}")]
        public Client Get(int id)
        {
            return _clientService.GetById(id);
        }

        // POST api/clients
        [HttpPost]
        public void Post([FromBody] Client client)
        {
            if(ModelState.IsValid)
            {
                _clientService.Create(client);
            }
        }

        // PUT api/clients/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Client client)
        {

            if(ModelState.IsValid)
            {
                client.Id = id;
                _clientService.Update(client);
            }
        }

        // DELETE api/clients/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _clientService.Delete(id);
        }
    }
}