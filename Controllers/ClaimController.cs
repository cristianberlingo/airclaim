using System.Collections.Generic;
using AirClaimAPI.Models;
using AirClaimAPI.Services.ServicesContracts;
using Microsoft.AspNetCore.Mvc;

namespace AirClaimAPI.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class ClaimController : ControllerBase
    {
        private readonly IBaseService<Claim> _claimService;

        public ClaimController(IBaseService<Claim> service)
        {
            _claimService = service;
        }

        // GET api/claims
        [HttpGet]
        public List<Claim> Get()
        {
            return _claimService.Get();
        }

        // GET api/claims/5
        [HttpGet("{id}")]
        public Claim Get(int id)
        {
            return _claimService.GetById(id);
        }

        // POST api/claims
        [HttpPost]
        public void Post([FromBody] Claim claim)
        {
            if(ModelState.IsValid)
            {
                _claimService.Create(claim);
            }
        }

        // PUT api/claims/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Claim claim)
        {

            if(ModelState.IsValid)
            {
                claim.Id = id;
                _claimService.Update(claim);
            }
        }

        // DELETE api/claims/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _claimService.Delete(id);
        }
    }
}