# Capa Controller

Esta capa contiene todas las clases que manejaran las peticiones HTTP y no debe implementar logica de negocio o llamar a la capa de de datos.

## Implementación por convención de ruteo
Mientras en otros Frameworks web como Express.js de Node o Django de Python implementan una o multiples clases Routers que explicitamente definen que metodo de que clase se va a responsabilizar de cada petición HTTP, en ASP.NET Core el comportamiento por defecto es definir que clase y metodo respondera a cada petición a travez de convenciones y Decorators.

### Resolución de clase encargada de cada recurso de la API
En ASP.NET Core cada recurso de la API sera por defecto manejado por la clase Controller que tenga el mismo nombre y termine con el sufijo Controller.cs.
Por ejemplo la petición ```https://localhost:5001/api/claim``` , que corresponde al recurso Claims, sera manejado por la clase Controllers/ClaimController.cs.

### Resolución del metodo del controller que respondera a la petición
Debemos tener en cuenta que toda petición HTTP ademas de su URL tiene un verbo (GET, POST, PUT, DELETE, etc.) y parametros en la url, por ejemplo el id del Claim.
Una vez ya resuelto que clase se encargara de las peticiones del recurso, el framework posteriormente definira que metodo de todos los existentes en ClaimController.cs se hara cargo a travez de Decorators. Veremos cada uno de estos ejemplos.

En el caso de la peticion ```GET https://localhost:5001/api/claim``` , usada para recuperar la lista de reclamos, el framework buscara en la clase ClaimController.cs algun metodo que tenga el Decorator ```[HttpGet]```, como es el caso de nuestro ejemplo:

```c#
    // GET api/claim
    [HttpGet]
    public List<Claim> Get()
    {
        return _claimService.Get();
    }
```

En el caso de la peticion ```GET https://localhost:5001/api/claim/id``` , utilizada para recuperar un reclamo en particular, el framework buscara en la clase ClaimController.cs algun metodo que tenga el Decorator [HttpGet("{id}")]```, como es el caso de nuestro ejemplo:

```c#
    // GET api/claim/5
    [HttpGet("{id}")]
    public Claim Get(int id)
    {
        return _claimService.GetById(id);
    }
```

En el caso de la peticion ```POST https://localhost:5001/api/claim``` , utilizada para recuperar un reclamo en particular, el framework buscara en la clase ClaimController.cs algun metodo que tenga el Decorator ```[HttpPost]```, como es el caso de nuestro ejemplo:

```c#
    // POST api/claim
    [HttpPost]
    public void Post([FromBody] Claim claim)
    {
        if(ModelState.IsValid)
        {
            _claimService.Create(claim);
        }
    }
```

En este caso ademas vemos el Decorator ```[FromBody]``` en la firma del metodo. Esto indica que la API espera en el cuerpo de la petición un JSON con los mismos atributos que la clase Claim.

![Post](../res/readme/controller01.png)