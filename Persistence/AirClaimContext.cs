﻿using System;
using System.Linq;
using AirClaimAPI.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AirClaimAPI.Persistence
{
    public partial class AirClaimContext : DbContext, IAirClaimContext
    {
        public AirClaimContext()
        {
        }

        public AirClaimContext(DbContextOptions<AirClaimContext> options)
            : base(options)
        {
        }

        public DbSet<Claim> Claim { get; set; }
        public DbSet<Client> Client { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            // set up properties like required/not null
            modelBuilder.Entity<Client>(client => 
            {
                client.Property(cl => cl.Dni).IsRequired();
                client.Property(cl => cl.FirstName).IsRequired();
                client.Property(cl => cl.LastName).IsRequired();
                client.Property(cl => cl.Address).IsRequired();
            });

            modelBuilder.Entity<Claim>(claim => 
            {
                claim.Property(clm => clm.ClientId).IsRequired();
                claim.Property(clm => clm.StatusId).IsRequired();
            });

            modelBuilder.Entity<ContactInformation>(contactInformation =>
            {
                contactInformation.Property(ci => ci.Description).IsRequired();
                contactInformation.Property(ci => ci.Value).IsRequired();
            });

            // seeding the database tables, this will happen only at database creation time
            modelBuilder.Entity<Client>().HasData(
                new {Id = 1, FirstName = "John", LastName = "Doe", Dni = 1234567, Address = "Fake street 123"},
                new {Id = 2, FirstName = "Jane", LastName = "Doe", Dni = 1234568, Address = "Fake street 321"} 
            );

            // seeding ClaimStatus table with ClaimStatusEnum values
            modelBuilder.Entity<ClaimStatus>().HasData(
                Enum.GetValues(typeof(ClaimStatusEnum))
                    .OfType<ClaimStatusEnum>()
                    .Select(e => new ClaimStatus() {Id = e, Name = e.ToString()})
                    .ToArray()
            );

            modelBuilder.Entity<Claim>().HasData(
                new {Id = 1, ClientId = 1, StatusId = ClaimStatusEnum.Consulta}
            );

            modelBuilder.Entity<ContactInformation>().HasData(
                new {Id = 1, ClaimId = 1, Description = "Cellphone", Value = "1130959722"},
                new {Id = 2, ClaimId = 1, Description = "Work's email", Value = "john.doe@gmail.com"}
            );
        }
    }
}
