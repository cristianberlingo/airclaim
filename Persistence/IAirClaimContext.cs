using AirClaimAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace AirClaimAPI.Persistence
{
    public interface IAirClaimContext: IDbContext
    {
        DbSet<Claim> Claim { get; set; }
        DbSet<Client> Client { get; set; }
    }
}