using System.Collections.Generic;
using AirClaimAPI.Models;

namespace AirClaimAPI.DAL.RepositoriesContracts
{
    public interface IClaimRepository : IRepository<Claim>
    {
    }
}