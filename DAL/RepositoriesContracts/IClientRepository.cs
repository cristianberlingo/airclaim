using System.Collections.Generic;
using AirClaimAPI.Models;

namespace AirClaimAPI.DAL.RepositoriesContracts
{
    public interface IClientRepository : IRepository<Client>
    {
    }
}