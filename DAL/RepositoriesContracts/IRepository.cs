using System.Collections.Generic;

namespace AirClaimAPI.DAL.RepositoriesContracts
{
    public interface IRepository<TEntity> where TEntity : class
    {
        List<TEntity> Get();
        TEntity GetById(int id);
        void Update(TEntity entity);
        void Delete(int id);
        void Insert(TEntity entity);
    }
}