using System.Collections.Generic;
using System.Linq;
using AirClaimAPI.DAL.RepositoriesContracts;
using AirClaimAPI.Models;
using AirClaimAPI.Persistence;
using Microsoft.EntityFrameworkCore;

namespace AirClaimAPI.DAL.Repositories
{
    public class ClaimRepository : IClaimRepository
    {

        private readonly IAirClaimContext _context;

        public ClaimRepository(IAirClaimContext context)
        {
            _context = context;
        }

        public void Delete(int id)
        {
            Claim claim = _context.Claim.Include(c => c.ContactInformation)
                .FirstOrDefault(c => c.Id == id);

            if(claim != null)
            {
                _context.Claim.Attach(claim);
                _context.Claim.Remove(claim);
                _context.SaveChanges();
            }
        }

        public List<Claim> Get()
        {
            List<Claim> claims = _context.Claim
                    .AsNoTracking()
                    .Include(cl => cl.Client)
                    .Include(cl => cl.ContactInformation)
                    .Include(cl => cl.Status)
                    .ToList();

            return claims;
        }

        public Claim GetById(int id)
        {
            return _context.Claim.AsNoTracking().FirstOrDefault(c => c.Id == id);
        }

        public void Insert(Claim claim)
        {
            _context.Claim.Add(claim);
            _context.SaveChanges();
        }

        public void Update(Claim claim)
        {
            _context.Claim.Update(claim);
            _context.SaveChanges();
        }
    }
}
