using System.Collections.Generic;
using System.Linq;
using AirClaimAPI.DAL.RepositoriesContracts;
using AirClaimAPI.Models;
using AirClaimAPI.Persistence;
using Microsoft.EntityFrameworkCore;

namespace AirClaimAPI.DAL.Repositories
{
    public class ClientRepository : IClientRepository
    {

        private readonly IAirClaimContext _context;

        public ClientRepository(IAirClaimContext context)
        {
            _context = context;
        }

        public void Delete(int id)
        {
            Client client = _context.Client.FirstOrDefault(c => c.Id == id);

            if(client != null)
            {
                _context.Client.Attach(client);
                _context.Client.Remove(client);
                _context.SaveChanges();
            }
        }

        public List<Client> Get()
        {
            return _context.Client.AsNoTracking().ToList();
        }

        public Client GetById(int id)
        {
            return _context.Client.AsNoTracking().FirstOrDefault(cl => cl.Id == id);            
        }

        public void Insert(Client client)
        {
            _context.Client.Add(client);
            _context.SaveChanges();
        }

        public void Update(Client client)
        {
            _context.Client.Update(client);
            _context.SaveChanges();
        }
    }
}