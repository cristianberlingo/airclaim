# DAL o Capa de acceso a los datos

Esta es la encargada de contener la logica para acceder a los datos. En nuestro ejemplo la unica fuente es la base de datos.

## Patron Repositorio
Esta capa implementa el Patron Repositorio que consiste en ofrecer una interfaz que media entre la persistencia de los datos y la logica de negocio (en nuestro caso la capa Service), esto permite asbtraer el resto de la capas de la aplicación de la tecnologia y logica que se utilice para acceder a los datos. Ademas mejora la capacidad de testeo de los metodos relacionados con la persistencia.

El Patron Repositorio es independiente de la tecnologia de la persitencia de datos, es un patron útil tanto si se trabaja con archivos, base de datos relacionales, no relacionales o conexiones a APIs de terceros u otros sistemas propios.

## Entity Framework Core
Entity Framework Core (EF o también EFC) es un [ORM](https://es.wikipedia.org/wiki/Mapeo_objeto-relacional) que nos permitira mapear las tablas de nuestra base de datos clases de C#, y a cada registro de nuestras tablas a objetos de nuestras clases.

Por ejemplo mapea la tabla client con la clase Client

```sql
CREATE TABLE [dbo].[Client](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](max) NOT NULL,
	[LastName] [nvarchar](max) NOT NULL,
	[Dni] [int] NOT NULL,
	[Address] [nvarchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [dbo].[Client] ADD  CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)
```

```c#
public class Client 
{

    public int Id { get; set; }
    public string FirstName {get; set;}
    public string LastName {get; set;}
    public int Dni {get; set;}
    public string Address {get; set;}
}
```

Y cada uno de sus registros con instancias de Client

![record](../res/readme/DAL02.png)

![instance](../res/readme/DAL01.png)

Tambien nos provee de una API para ejecutar queries sobre la base de datos sin tener que escribir codigo SQL.

![EFCode](../res/readme/DAL03.png)

Este codigo de EFC ejecutara una query como la siguiente:
```sql
SELECT claim.Id, claim.ClientId, claim.StatusId, 
       client.Id, client.FirstName, client.LastName, client.Dni, client.Address,
       contact.Id, contact.ClaimId, contact.[Description], contact.[Value],
       [status].Id, [status].[Name]
    FROM dbo.Claim claim
        INNER JOIN dbo.Client client
            ON claim.ClientId = client.Id
        INNER JOIN dbo.ContactInformation contact
            ON contact.ClaimId = claim.Id
        INNER JOIN dbo.ClaimStatus [status]
            ON claim.StatusId = [status].Id
```
Pero mapeando todos los registros del resultado de la query a un ```List<Claim>```, inclueyendo dentro de cada una de estas instancias sus atributos de tipo Client, ContactInformation y ClaimStatus que recupero en la query.

![listClaims](../res/readme/DAL05.png)

Entity Framework Core se ve representado en todos los Repositorios por el objeto _context que es una instancia de alguna clase que implemente la Interfaz ```IAirClaimContext``` y herada de la clase [DbContext](https://docs.microsoft.com/en-us/dotnet/api/microsoft.entityframeworkcore.dbcontext?view=efcore-2.1), la cual es la clase de Entity Framework a través de la que realizaremos las consultas sobre la base.

![_context](../res/readme/DAL04.png)
