using System.Collections.Generic;

namespace AirClaimAPI.Models
{
    public class Claim
    {

        public int Id { get; set; }
        public int ClientId {get; set;}
        public Client Client { get; set; }
        public List<ContactInformation> ContactInformation {get; set;}
        public ClaimStatusEnum StatusId { get; set; }
        public ClaimStatus Status { get; set; }

    }

    public class ContactInformation
    {

        public int Id { get; set; }
        public string Description {get; set;}
        public string Value {get; set;}

    }
}