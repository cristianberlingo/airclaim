namespace AirClaimAPI.Models
{
    public enum ClaimStatusEnum
    {
        Consulta = 1,
        ReclamoViable = 2,
        EnPreparacion = 3,
        EnMediacion = 4,
        EnNegociacion = 5,
        FinalizadoAcuerdo = 6,
        PendientePago = 7,
        Judicializacion = 8,
        FinalizadoJudicial = 9,
        Pago = 10,
        CierreSinAcuerdo = 11,
        FinalizadoOtro = 12,
        ArchivoPapelera = 13
    }

    public class ClaimStatus
    {
        public ClaimStatusEnum Id {get; set;}
        public string Name { get; set; }
    }
}