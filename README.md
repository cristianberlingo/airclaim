# Scaffolding AirClaim

Este proyecto tiene como objetivo definir cual sera la arquitectura a seguir en el desarrollo de la API del bakcend.
Su objetivo no es ser una implementación fiel de la logica del negocio. Los nombres utilizados propios del negocio (Claim\Reclamo\Caso, Client) fueron utilizados para dar una mayor claridad a el ejemplo.

## Requisitos
- Sistema Operativo: Cualquiera. Todas las tecnologias son multiplataformas.
- SQL Server: El proyecto fue probado con SQL Server 2017, pero otras versiones deberian funcionar tambien.
- ASP .NET Core 2.2.

## Instalación
```bash
git clone https://gitlab.com/cristianberlingo/airclaim.git
cd airclaim
```

## Definicion de la conexion a la base de datos
El conection string se define en el archivo appsettings.json

```json
"ConnectionStrings": {
    "DefaultConnection": "SERVER=localhost,1434;DATABASE=AirClaim;UID=sa;PWD=AirClaim2019"
  },
```
No hay inconveniente si la base de datos no existe, esta sera creada por Entity Framework.

### Aclaración
En el atributo SERVER de la connection string se utiliza la ',' para definir en que puerto esta escuchando esta instancia del motor de base de datos. Por defecto SQL Server siempre escucha en el puerto 1433, yo especifique 1434 por que estaba usando una instancia del motor que escuchaba en ese puerto. Tambien si es necesario se puede usar alias, por ejemplo si instalaron SQL Server Express seguramente les haya creado una instancia llamada "SQLEXPRESS", para referenciar a esta instancia el conection string tendria este valor **"SERVER=localhost\\SQLEXPRESS;..."**.

## Dotnet
La herramienta CLI **dotnet** es la herramienta que nos permitira manejar el proyecto asi como tambien las versiones de la base de datos. Se instala por defecto junto con ASP .NET Core y es indenpediente del IDE que se use. Por defecto siempre considera que sus comandos hacen referencia a la carpeta donde se esta situado y buscara en esa carpeta un archivo de extension .csproj que le defina a cual proyecto se esta haciendo referencia.

## Creación de la base de datos con dotnet y Entity Framework
El siguiente comando creara la base de datos tomando lo definido en la ultima migracion (hecha por mi antes de subir el proyecto)
```bash 
dotnet ef database update
```

## Ejecución de la aplicación
Para correr la aplicación debemos ejecutar el siguiente comando.
```bash
dotnet run
```
Este comando instalara las dependencias del proyecto y pondra a correr la API.

## Resumen
En resumen para instalar, crear la base de datos y correr la API debemos ejecutar los siguientes comandos.
```bash
git clone https://gitlab.com/cristianberlingo/airclaim.git
cd airclaim
dotnet ef database update
dotnet run
```

# Resultado y estado inicial
Al correr ```dotnet run``` la API ya esta en ejecución y puede ser probada de esta forma:

![Peticion](res/readme/controller01.png)
